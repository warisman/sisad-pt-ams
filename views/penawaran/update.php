<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Penawaran */

$this->title = 'Update Penawaran: ' . $model->id_penawaran;
$this->params['breadcrumbs'][] = ['label' => 'Penawaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_penawaran, 'url' => ['view', 'id' => $model->id_penawaran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penawaran-update">

    <?= $this->render('_form', [
        'model' => $model,
        'itemPenawaran' => $itemPenawaran,
    ]) ?>

</div>
