<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Penawaran */

$this->title = 'Create Penawaran';
$this->params['breadcrumbs'][] = ['label' => 'Penawaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penawaran-create">

    <?= $this->render('_form', [
    'model' => $model,
    'itemPenawaran' => $itemPenawaran,
    ]) ?>

</div>
