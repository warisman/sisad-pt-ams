<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\PenawaranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penawaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_penawaran') ?>

    <?= $form->field($model, 'id_customer') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'tempo_bayar') ?>

    <?= $form->field($model, 'pnp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
