<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PenawaranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penawaran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penawaran-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Penawaran', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id_penawaran',
                'customer.nama_customer',
                // 'tanggal',
                // 'tempo_bayar',
                // 'pnp',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{penawaran} {view} {update} {delete}',
                    'buttons'=>[
                        'view'=>function ($url) { return Html::a('Detail', $url, ['class' => 'btn btn-info btn-sm']); },
                        'update'=>function ($url) { return Html::a('Update', $url, ['class' => 'btn btn-warning btn-sm']); },
                        'delete'=>function ($url) { return Html::a('Hapus', $url, ['class' => 'btn btn-danger btn-sm']); },
                        'penawaran' => function ($url, $model) {
                            return Html::a(
                                'Create Penjualan',
                                ['penjualan/create-by-penawaran', 'id_penawaran' => $model->id_penawaran],
                                ['class' => 'btn btn-default btn-sm']
                            );
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
