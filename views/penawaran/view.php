<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Penawaran */

$this->title = $model->id_penawaran;
$this->params['breadcrumbs'][] = ['label' => 'Penawaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penawaran-view box box-primary no-print">
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id_penawaran], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id_penawaran], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cetak Penawaran', null, [
                'class' => 'btn btn-info btn-flat pull-right',
                'onclick' => 'window.print();return false;',
            ]
        )?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id_penawaran',
                'id_customer',
                'tanggal',
                'tempo_bayar',
                'pnp',
            ],
        ]) ?>
        <?php foreach ($model->itemPenawarans as $itemData) { ?>
        <div class="col-md-4">
            <div class="box box-default collapsed-box">
                <div class="box-header with-border" data-widget="collapse">
                    <p><?=$itemData->kodeItem->nama_item?></p>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $itemData,
                        'attributes' => [
                            'kode_item',
                            'qty',
                            'harga_satuan',
                        ],
                    ]);?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<div class="wrapper hide-on-screen">
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-file"></i> Nota Penawaran
                    <small class="pull-right">Tanggal Penawaran : <?=date('d M y', strtotime($model->tanggal))?></small>
                </h2>
            </div>
        </div>
        <div class="row invoice-info">
            <div class="col-sm-12">
                <address>
                    <table>
                        <tr>
                            <th>ID Penawaran &emsp; &emsp; &emsp;</th>
                            <th> : <?=$model->id_penawaran?></th>
                        </tr>
                        <tr>
                            <th>Nama Customer &emsp; &emsp; &emsp;</th>
                            <td> : <?=$model->customer->nama_customer?></td>
                        </tr>
                        <tr>
                            <th>PNP &emsp; &emsp; &emsp;</th>
                            <td> : <?=$model->pnp?></td>
                        </tr>
                        <tr>
                            <th>Tempo Bayar &emsp; &emsp; &emsp;</th>
                            <td> : <?=$model->tempo_bayar?></td>
                        </tr>
                    </table>
                </address>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Qty</th>
                            <th>Kode Item</th>
                            <th>Nama Item</th>
                            <th>Harga Satuan</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sub_total = 0;
                        foreach ($model->itemPenawarans as $itemData) {
                        $sub_total += $itemData->harga_satuan;
                    ?>
                        <tr>
                            <td><?=$itemData->qty?></td>
                            <td><?=$itemData->kode_item?></td>
                            <td><?=$itemData->kodeItem->nama_item?></td>
                            <td><?=$itemData->harga_satuan?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" class="text-right"><strong>Sub Total</strong></td>
                            <td><?=$sub_total?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
</div>