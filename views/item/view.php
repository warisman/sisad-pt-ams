<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Item */

$this->title = $model->kode_item;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-view box box-primary">
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->kode_item], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->kode_item], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'kode_item',
                'nama_item',
                'satuan',
                'kemasan',
                'harga_beli',
                'harga_jual',
                'keterangan',
            ],
        ]) ?>
    </div>
</div>
