<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'kode_item')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'nama_item')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'satuan')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'kemasan')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'harga_beli')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'harga_jual')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
