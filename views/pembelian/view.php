<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pembelian */

$this->title = $model->id_pembelian;
$this->params['breadcrumbs'][] = ['label' => 'Pembelian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pembelian-view box box-primary">
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id_pembelian], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id_pembelian], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id_pembelian',
                'kode_suplier',
                'tanggal',
                'no_faktur',
                'top_bel',
                'dp',
                'pnp',
                'no_faktur_pajak',
            ],
        ]) ?>
        <?php foreach ($model->itemPembelians as $itemData) { ?>
        <div class="col-md-4">
            <div class="box box-default collapsed-box">
                <div class="box-header with-border" data-widget="collapse">
                    <p><?=$itemData->kodeItem->nama_item?></p>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $itemData,
                        'attributes' => [
                            'kode_item',
                            'qty',
                            'harga_satuan',
                        ],
                    ]);?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
