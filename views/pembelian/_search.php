<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\PembelianSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pembelian-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pembelian') ?>

    <?= $form->field($model, 'kode_suplier') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'no_faktur') ?>

    <?= $form->field($model, 'top_bel') ?>

    <?php // echo $form->field($model, 'dp') ?>

    <?php // echo $form->field($model, 'pnp') ?>

    <?php // echo $form->field($model, 'no_faktur_pajak') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
