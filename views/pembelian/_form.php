<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Suplier;
use app\models\Item;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Pembelian */
/* @var $form yii\widgets\ActiveForm */

$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-itemPembelian").each(function(index) {
        jQuery(this).html("Barang: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-itemPembelian").each(function(index) {
        jQuery(this).html("Barang: " + (index + 1))
    });
});
';
$this->registerJs($js);
?>

<div class="pembelian-form box box-primary">
    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <?php if ($model->hasErrors()) { ?>
    <div class="callout callout-warning">
    <?= $form->errorSummary($model); ?>
    </div>
    <?php } ?>
    <div class="box-body">
        <div class="col-md-4">

            <?= $form->field($model, 'id_pembelian')->textInput(['maxlength' => true, 'readOnly'=> true]) ?>

            <?= $form->field($model, 'kode_suplier')->dropDownList(
                ArrayHelper::map(Suplier::find()->all(),'kode_suplier','nama_suplier'), ['prompt' => '', 'class' => 'form-control select2']
            )->label('Supplier') ?>

            <?php
                // necessary for update action.
                if (!$model->isNewRecord) {
                    echo Html::input('text', 'tanggal', date("d-m-Y", strtotime($model->tanggal)), ['class' => 'form-control']);
                } else {
                    echo $form->field($model, 'tanggal')->input('date');
                }
            ?>

            <?= $form->field($model, 'no_faktur')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'top_bel')->dropDownList(
                    [
                        'COD' => 'COD',
                        '30' => '30',
                        '45' => '45',
                    ]
            ); ?>

            <?= $form->field($model, 'dp')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'pnp')->dropDownList(
                    [
                        'P' => 'Pajak',
                        'N' => 'Non Pajak',
                    ]
            ); ?>

            <?= $form->field($model, 'no_faktur_pajak')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-8">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $itemPembelian[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'kode_item',
                    'id_pembelian',
                ],
            ]); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-list"></i> Daftar Barang
                    <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Tambah Barang</button>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body container-items"><!-- widgetContainer -->
                    <?php foreach ($itemPembelian as $index => $item): ?>
                        <?php if ($item->hasErrors()) { ?>
                        <div class="callout callout-warning">
                        <?= $form->errorSummary($item); ?>
                        </div>
                        <?php } ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <span class="panel-title-itemPembelian">Barang: <?= ($index + 1) ?></span>
                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    // necessary for update action.
                                    if (!$item->isNewRecord) {
                                        echo Html::activeHiddenInput($item, "[{$index}]id_pembelian");
                                    }
                                ?>
                                <div class="col-md-7">
                                <?= $form->field($item, "[{$index}]kode_item")->dropDownList(
                                    ArrayHelper::map(Item::find()->all(),'kode_item',
                                        function($model) {
                                            return $model['kode_item'].' - '.$model['nama_item'];
                                        }
                                    ),
                                    ['prompt' => '', 'class' => 'form-control select2']
                                    )->label('Barang') ?>
                                </div>
                                <div class="col-md-3">
                                <?= $form->field($item, "[{$index}]harga_satuan")->textInput(['maxlength' => true, 'type' => 'number']) ?>
                                </div>
                                <div class="col-md-2">
                                <?= $form->field($item, "[{$index}]qty")->textInput(['maxlength' => true, 'type' => 'number']) ?>
                                </div>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>
    <div class="box-footer text-center">
        <?= Html::submitButton('Save', ['class' => 'btn btn-lg btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJs(
    "$('.select2').select2();"
);
?>