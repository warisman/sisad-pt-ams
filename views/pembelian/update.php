<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pembelian */

$this->title = 'Update Pembelian: ' . $model->id_pembelian;
$this->params['breadcrumbs'][] = ['label' => 'Pembelian', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pembelian, 'url' => ['view', 'id' => $model->id_pembelian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pembelian-update">

    <?= $this->render('_form', [
        'model' => $model,
        'itemPembelian' => $itemPembelian,
    ]) ?>

</div>
