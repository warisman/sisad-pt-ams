<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Customer', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id_customer',
                'kode',
                'nama_customer',
                'npwp',
                'alamat',
                // 'alamat_surat_jalan',
                // 'telp',
                // 'fax',
                // 'email:email',
                // 'to_',
                // 'cc',
                // 'contact_person',
                // 'akronim',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons'=>[
                        'view'=>function ($url) { return Html::a('Detail', $url, ['class' => 'btn btn-info btn-sm']); },
                        'update'=>function ($url) { return Html::a('Update', $url, ['class' => 'btn btn-warning btn-sm']); },
                        'delete'=>function ($url) { return Html::a('Hapus', $url, ['class' => 'btn btn-danger btn-sm']); },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
