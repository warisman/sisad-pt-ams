<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-12 text-center">
                <img src="<?=Yii::$app->params['icon']?>" alt="logo">
            </div>
            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="col-lg-12">
                <h1 align="center">Sistem Administrasi PT. AMS</h1>
                <br>
                <h2>Sebuah sistem administrasi terkomputerisasi berbasis web untuk PT. AMS. <br> Fitur yang dikembangkan meliputi:</h2>
                <h3>Master Data</h3>
                <p>Pengolahan master data meliputi:</p>
                <ol>
                    <li>Kategori Barang. Kategori barang digunakan untuk mempermudah proses pencarian data barang.</li>
                    <li>Barang</li>
                    <li>Supplier</li>
                    <li>Pelanggan</li>
                    <li>Freelance Marketing. Freelance marketing merujuk kepada orang atau pihak yang mendatangkan pekerjaan bagi PT AMS dan diberikan komisi oleh PT AMS.</li>
                </ol>
                <h3>Transaksi</h3>
                <p>Transaksi meliputi:</p>
                <ol>
                    <li>Pembelian</li>
                    <li>Penjualan. Pada transaksi penjualan terdapat beberapa fitur sebagai berikut:
                        <ul>
                            <li>Pencarian data pelanggan</li>
                            <li>Pencarian data barang</li>
                            <li>Entri data barang</li>
                            <li>Kalkulator untuk menentukan nilai jual</li>
                            <li>Cetak nota penjualan</li>
                            <li>Selain itu transaksi penjualan juga dapat mengakomodir transaksi baik tunai maupun non tunai, transaksi dengan diskon maupun non diskon serta transaksi PPN dan non PPN.</li>
                        </ul>
                    </li>
                    <li>Hutang. Fitur untuk menginputkan data transaksi pembayaran hutang dan Piutang. Fitur untuk menginputkan data transaksi penerimaan piutang.</li>
                </ol>
                <h3>Pelaporan</h3>
                <p>Pelaporan meliputi:</p>
                <ol>
                    <li>Laporan pembelian (Harian, Bulanan, Berkala atau pada periode
                        tertentu)
                        <ul>
                            <li>Detail per transaksi pembelian.</li>
                            <li>Rekapitulasi per barang.</li>
                            <li>Rekapitulasi per supplier.</li>
                        </ul>
                    </li>
                    <li>Laporan Penjualan (Harian, Bulanan, Berkala atau pada periode
                        tertentu)
                        <ul>
                            <li>Detail per transaksi penjualan</li>
                            <li>Rekapitulasi per barang.</li>
                            <li>Rekapitulasi per pelanggan</li>
                            <li>Laporan Hutang</li>
                            <li>Detail per transaksi</li>
                            <li>Rekapitulasi per supplier</li>
                        </ul>
                    </li>
                    <li>Laporan Piutang
                        <ul>
                            <li>Detail per transaksi penjualan</li>
                            <li>Rekapitulasi per pelanggan</li>
                        </ul>
                    </li>
                    <li>Analisis frekuensi belanja pelanggan yang dilengkapi dengan fitur pembuatan surat penawaran.</li>
                </ol>
            </div>
            <?php } ?>
        </div>
    </div>
</div>