<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Beranda', 'icon' => 'home', 'url' => ['/']],
                    [
                        'label' => 'Master Data',
                        'icon' => 'folder-o',
                        'url' => '#',
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'Item', 'icon' => 'truck', 'url' => ['/item']],
                            ['label' => 'Supplier', 'icon' => 'industry', 'url' => ['/supplier']],
                            ['label' => 'Customer', 'icon' => 'group', 'url' => ['/customer']],
                            ['label' => 'Penerima Komisi', 'icon' => 'money', 'url' => ['/penerima-komisi']],
                        ],
                    ],
                    [
                        'label' => 'Transaksi',
                        'icon' => 'retweet',
                        'url' => '#',
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'Pembelian', 'icon' => 'plus-square', 'url' => ['/pembelian']],
                            ['label' => 'Penawaran', 'icon' => 'thumbs-o-up', 'url' => ['/penawaran']],
                            ['label' => 'Penjualan', 'icon' => 'minus-square', 'url' => ['/penjualan']],
                            ['label' => 'Hutang', 'icon' => 'mail-reply-all', 'url' => ['/hutang']],
                        ],
                    ],
                    [
                        'label' => 'Pelaporan',
                        'icon' => 'calendar',
                        'url' => '#',
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'Pembelian', 'url' => ['/pelaporan/pembelian']],
                            ['label' => 'Penjualan', 'url' => ['/pelaporan/penjualan']],
                            // ['label' => 'Piutang', 'url' => ['/pelaporan/piutang']],
                            // ['label' => 'Analisa', 'url' => ['/pelaporan/analisa']],
                        ],
                    ],
                    ['label' => 'Sign In', 'icon' => 'sign-in', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'User Management', 'icon' => 'wrench', 'url' => ['/user-management'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'Sign Out', 'icon' => 'sign-out', 'url' => ['site/logout'], 'visible' => !Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
