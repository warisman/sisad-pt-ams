<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'username',
                'date_created',
                'date_modified',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons'=>[
                        'view'=>function ($url) { return Html::a('Detail', $url, ['class' => 'btn btn-info btn-sm']); },
                        'update'=>function ($url) { return Html::a('Update', $url, ['class' => 'btn btn-warning btn-sm']); },
                        'delete'=>function ($url) { return Html::a('Hapus', $url, ['class' => 'btn btn-danger btn-sm']); },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
