<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Suppliers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supplier-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Supplier', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'kode_suplier',
                'nama_suplier',
                'npwp',
                'alamat',
                'telp_fax',
                // 'up',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons'=>[
                        'view'=>function ($url) { return Html::a('Detail', $url, ['class' => 'btn btn-info btn-sm']); },
                        'update'=>function ($url) { return Html::a('Update', $url, ['class' => 'btn btn-warning btn-sm']); },
                        'delete'=>function ($url) { return Html::a('Hapus', $url, ['class' => 'btn btn-danger btn-sm']); },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
