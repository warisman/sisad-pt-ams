<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PenjualanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penjualan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penjualan-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Penjualan', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        <?= Html::a('Create Penjualan dari Penawaran', ['/penawaran/index'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id_penjualan',
                'customer.nama_customer',
                // 'pnp',
                // 'po',
                'tanggal',
                // 'dp',
                // 'tempo_bayar',
                // 'dari_penawaran',
                // 'diskon',
                // 'biaya_admin',
                // 'biaya_transport',
                // 'ongkos_kirim',
                // 'bonta',
                // 'pph_final',
                // 'profit',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons'=>[
                        'view'=>function ($url) { return Html::a('Detail', $url, ['class' => 'btn btn-info btn-sm']); },
                        'update'=>function ($url) { return Html::a('Update', $url, ['class' => 'btn btn-warning btn-sm']); },
                        'delete'=>function ($url) { return Html::a('Hapus', $url, ['class' => 'btn btn-danger btn-sm']); },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
