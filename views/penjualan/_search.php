<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\PenjualanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penjualan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_penjualan') ?>

    <?= $form->field($model, 'id_customer') ?>

    <?= $form->field($model, 'pnp') ?>

    <?= $form->field($model, 'po') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?php // echo $form->field($model, 'dp') ?>

    <?php // echo $form->field($model, 'tempo_bayar') ?>

    <?php // echo $form->field($model, 'dari_penawaran') ?>

    <?php // echo $form->field($model, 'diskon') ?>

    <?php // echo $form->field($model, 'biaya_admin') ?>

    <?php // echo $form->field($model, 'biaya_transport') ?>

    <?php // echo $form->field($model, 'ongkos_kirim') ?>

    <?php // echo $form->field($model, 'bonta') ?>

    <?php // echo $form->field($model, 'pph_final') ?>

    <?php // echo $form->field($model, 'profit') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
