<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Penjualan */

$this->title = 'Update Penjualan: ' . $model->id_penjualan;
$this->params['breadcrumbs'][] = ['label' => 'Penjualan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_penjualan, 'url' => ['view', 'id' => $model->id_penjualan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penjualan-update">

    <?= $this->render('_form', [
        'model' => $model,
        'komisi' => $komisi,
        'itemPenjualan' => $itemPenjualan,
    ]) ?>

</div>
