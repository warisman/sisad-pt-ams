<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Customer;
use app\models\PenerimaKomisi;
use app\models\Item;
use wbraganca\dynamicform\DynamicFormWidget;

if ($this->context->action->id == 'create-by-penawaran') {
    Yii::$app->view->title = 'Create Penjualan dari Panawaran';
}

/* @var $this yii\web\View */
/* @var $model app\models\Penjualan */
/* @var $form yii\widgets\ActiveForm */

$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-itemPenjualan").each(function(index) {
        jQuery(this).html("Barang: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-itemPenjualan").each(function(index) {
        jQuery(this).html("Barang: " + (index + 1))
    });
});
';
$this->registerJs($js);
?>

<div class="penjualan-form box box-primary">
    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <?php if ($model->hasErrors()) { ?>
    <div class="callout callout-warning">
    <?= $form->errorSummary($model); ?>
    </div>
    <?php } ?>
    <div class="box-body">
        <div class="col-md-4">

            <?= ($this->context->action->id == 'create-by-penawaran') ? $form->field($model, 'id_penawaran')->textInput(['maxlength' => true, 'readOnly'=> true]) : ''; ?>

            <?= $form->field($model, 'id_penjualan')->textInput(['maxlength' => true, 'readOnly'=> true]) ?>

            <?= $form->field($model, 'id_customer')->dropDownList(
                ArrayHelper::map(Customer::find()->all(),'id_customer','nama_customer'), ['prompt' => '', 'class' => 'form-control select2', 'disabled'=> ($this->context->action->id == 'create-by-penawaran') ? true : false]
            )->label('Customer') ?>

            <?= $form->field($model, 'pnp')->dropDownList(
                    [
                        'P' => 'Pajak',
                        'N' => 'Non Pajak',
                    ],
                    ['disabled'=> ($this->context->action->id == 'create-by-penawaran') ? true : false]
            ); ?>

            <?= $form->field($model, 'po')->textInput(['maxlength' => true]) ?>

            <?php
                // necessary for update action.
                if (!$model->isNewRecord) {
                    echo Html::input('text', 'tanggal', date("d-m-Y", strtotime($model->tanggal)), ['class' => 'form-control']);
                } else {
                    echo $form->field($model, 'tanggal')->input('date');
                }
            ?>

            <?= $form->field($model, 'dp')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'tempo_bayar')->dropDownList(
                    [
                        'COD' => 'COD',
                        '30' => '30',
                        '45' => '45',
                    ]
            ); ?>

            <?= $form->field($model, 'diskon')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'biaya_admin')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'biaya_transport')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ongkos_kirim')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'bonta')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'pph_final')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'profit')->textInput(['maxlength' => true]) ?>

            <?= $form->field($komisi, 'id_penerima')->dropDownList(
                ArrayHelper::map(PenerimaKomisi::find()->all(),'id_penerima','nama_penerima'), ['prompt' => '', 'class' => 'form-control select2']
            )->label('Penerima Komisi') ?>

            <?= $form->field($komisi, 'jumlah_persen')->textInput(['maxlength' => true]) ?>

            <?= $form->field($komisi, 'jumlah_nominal')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-8">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $itemPenjualan[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'kode_item',
                    'id_penjualan',
                ],
            ]); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-list"></i> Daftar Barang
                    <button type="button" class="<?=($this->context->action->id == 'create-by-penawaran') ? 'hidden' : ''?> pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Tambah Barang</button>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body container-items"><!-- widgetContainer -->
                    <?php foreach ($itemPenjualan as $index => $item): ?>
                        <?php if ($item->hasErrors()) { ?>
                        <div class="callout callout-warning">
                        <?= $form->errorSummary($item); ?>
                        </div>
                        <?php } ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <span class="panel-title-itemPenjualan">Barang: <?= ($index + 1) ?></span>
                                <button type="button" class="<?=($this->context->action->id == 'create-by-penawaran') ? 'hidden' : ''?> pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    // necessary for update action.
                                    if (!$item->isNewRecord) {
                                        echo Html::activeHiddenInput($item, "[{$index}]id_penjualan");
                                    }
                                ?>
                                <div class="col-md-7">
                                <?= $form->field($item, "[{$index}]kode_item")->dropDownList(
                                    ArrayHelper::map(Item::find()->all(),'kode_item','nama_item'), ['prompt' => '', 'class' => 'form-control select2']
                                    )->label('Barang') ?>
                                </div>
                                <div class="col-md-3">
                                <?= $form->field($item, "[{$index}]harga_satuan")->textInput(['maxlength' => true, 'type' => 'number']) ?>
                                </div>
                                <div class="col-md-2">
                                <?= $form->field($item, "[{$index}]qty")->textInput(['maxlength' => true, 'type' => 'number']) ?>
                                </div>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>
    <div class="box-footer text-center">
        <?= Html::submitButton('Save', ['class' => 'btn btn-lg btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJs(
    "$('.select2').select2();"
);
?>