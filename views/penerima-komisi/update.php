<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PenerimaKomisi */

$this->title = 'Update Penerima Komisi: ' . $model->id_penerima;
$this->params['breadcrumbs'][] = ['label' => 'Penerima Komisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_penerima, 'url' => ['view', 'id' => $model->id_penerima]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penerima-komisi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
