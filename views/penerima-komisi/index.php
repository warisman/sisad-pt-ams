<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PenerimaKomisiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penerima Komisis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penerima-komisi-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Penerima Komisi', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id_penerima',
                'nama_penerima',
                'npwp',
                'telp',
                'alamat',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons'=>[
                        'view'=>function ($url) { return Html::a('Detail', $url, ['class' => 'btn btn-info btn-sm']); },
                        'update'=>function ($url) { return Html::a('Update', $url, ['class' => 'btn btn-warning btn-sm']); },
                        'delete'=>function ($url) { return Html::a('Hapus', $url, ['class' => 'btn btn-danger btn-sm']); },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
