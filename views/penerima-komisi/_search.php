<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\PenerimaKomisiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penerima-komisi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_penerima') ?>

    <?= $form->field($model, 'nama_penerima') ?>

    <?= $form->field($model, 'npwp') ?>

    <?= $form->field($model, 'telp') ?>

    <?= $form->field($model, 'alamat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
