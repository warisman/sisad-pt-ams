<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PenerimaKomisi */

$this->title = 'Create Penerima Komisi';
$this->params['breadcrumbs'][] = ['label' => 'Penerima Komisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penerima-komisi-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
