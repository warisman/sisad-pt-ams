<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Laporan Pembelian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="well">
                    <?= Html::beginForm(null, 'get', ['id' => 'range-form', 'class' => 'form-inline']) ?>
                    <div class="form-group">
                        <div class="input-group">
                            <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                                <span><i class="fa fa-calendar"></i> Rentang Waktu</span>
                                <i class="fa fa-caret-down"></i>
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Html::input('text', 'start', $startDate, ['readonly' => '', 'id' => 'start', 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <strong>-</strong>
                        <?= Html::input('text', 'end', $endDate, ['readonly' => '', 'id' => 'end', 'class' => 'form-control']) ?>
                    </div>
                    <?php Html::endForm() ?>

                    <?= Html::a('Download Laporan',
                        [
                            'download-pembelian',
                            'start' => $startDate,
                            'end' => $endDate,
                        ],
                        [
                            'class' => 'btn btn-info btn-flat pull-right',
                        ]
                    )?>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{summary}\n{items}\n{summary}",
                    'tableOptions' => [
                        'class' => 'table report-table report-table-bordered table-condensed',
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id_pembelian',
                        'kode_suplier',
                        'tanggal',
                        'no_faktur',
                        'top_bel',
                        'dp',
                        'pnp',
                        'no_faktur_pajak',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{lihat}{download}{print}',
                            'buttons'=>[
                                'download' => function ($url, $model) {
                                    return Html::a(
                                        'Download Detail',
                                        ['pelaporan/download-pembelian', 'id_pembelian' => $model->id_pembelian],
                                        ['class' => 'btn btn-default btn-sm']
                                    );
                                },
                                'lihat' => function ($url, $model) {
                                    return Html::a(
                                        'Lihat Detail',
                                        ['pembelian/view', 'id' => $model->id_pembelian],
                                        ['class' => 'btn btn-info btn-sm', 'target' => "_blank"]
                                    );
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(
    "$('#daterange-btn').daterangepicker(
      {
        opens: 'right',
        autoApply: true,
        ranges   : {
          'Hari ini'        : [moment(), moment()],
          'Kemarin'         : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '7 Hari terakhir' : [moment().subtract(6, 'days'), moment()],
          '30 Hari terakhir': [moment().subtract(29, 'days'), moment()],
          '1 Tahun terakhir': [moment().subtract(365, 'days'), moment()],
          'Bulan ini'       : [moment().startOf('month'), moment().endOf('month')],
          'Bulan lalu'      : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        locale: {
          format: 'YYYY-MM-DD'
        },
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#start').val(start.format('YYYY-MM-DD'));
        $('#end').val(end.format('YYYY-MM-DD'));
        $('#range-form').submit();
      }
    )
    $('#daterange-btn').data('daterangepicker').setStartDate('".$startDate."');
    $('#daterange-btn').data('daterangepicker').setEndDate('".$endDate."');
    "
);
?>