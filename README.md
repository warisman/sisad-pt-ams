
<p align="center">
    <h1 align="center">Sistem Administrasi PT. AMS</h1>
    <br>
</p>

## Sebuah sistem administrasi terkomputerisasi berbasis web untuk PT. AMS.

## Fitur yang dikembangkan meliputi:
### 1) Master Data
Pengolahan master data meliputi:
1. Kategori Barang/Item. Kategori barang digunakan untuk mempermudah proses pencarian data barang.
2. Barang/Item (Done)
3. Supplier (Done)
4. Pelanggan/Customer (Done)
5. Freelance Marketing. Freelance marketing merujuk kepada orang atau pihak yang mendatangkan pekerjaan bagi PT AMS dan diberikan komisi oleh PT AMS.

### 2) Transaksi
Transaksi meliputi:
1. Pembelian
2. Penjualan. Pada transaksi penjualan terdapat beberapa fitur sebagai berikut:
   * Pencarian data pelanggan
   * Pencarian data barang
   * Entri data barang
   * Kalkulator untuk menentukan nilai jual
   * Cetak nota penjualan
   * Selain itu transaksi penjualan juga dapat mengakomodir transaksi baik tunai maupun non tunai, transaksi dengan diskon maupun non diskon serta transaksi PPN dan non PPN.
3. Hutang. Fitur untuk menginputkan data transaksi pembayaran hutang dan Piutang. Fitur untuk menginputkan data transaksi penerimaan piutang.

### 3) Pelaporan
Pelaporan meliputi:
1. Laporan pembelian (Harian, Bulanan, Berkala atau pada periode
tertentu)
   * Detail per transaksi pembelian.
   * Rekapitulasi per barang.
   * Rekapitulasi per supplier.
2. Laporan Penjualan (Harian, Bulanan, Berkala atau pada periode
tertentu)
   * Detail per transaksi penjualan
   * Rekapitulasi per barang.
   * Rekapitulasi per pelanggan
   * Laporan Hutang
   * Detail per transaksi
   * Rekapitulasi per supplier
3. Laporan Piutang
   * Detail per transaksi penjualan
   * Rekapitulasi per pelanggan
5. Analisis frekuensi belanja pelanggan yang dilengkapi dengan fitur pembuatan surat penawaran.