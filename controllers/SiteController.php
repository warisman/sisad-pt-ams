<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Login;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
        ];
    }

    public function actionIndex()
    {
        $page_name = 'Beranda';
        Yii::$app->view->title = $page_name.' | '. Yii::$app->name;

        return $this->render('index', [
            'page_name' => $page_name,
        ]);
    }

    public function actionLogin()
    {
        // print_r(Yii::$app->security->generatePasswordHash('qwe123').PHP_EOL);
        // print_r(Yii::$app->security->generateRandomString().PHP_EOL);
        // print_r(Yii::$app->security->generateRandomString() . '_' . time());
        // die();
        $this->layout = 'main-login';
        $page_name = 'Sign In';
        Yii::$app->view->title = $page_name.' | '. Yii::$app->name;

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
            'page_name' => $page_name,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
