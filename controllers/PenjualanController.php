<?php

namespace app\controllers;

use Yii;
use app\models\Penjualan;
use app\models\Penawaran;
use app\models\Komisi;
use app\models\ItemPenjualan;
use app\models\search\PenjualanSearch;
use app\components\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class PenjualanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new PenjualanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Penjualan;
        $komisi = new Komisi;
        $itemPenjualan = [new ItemPenjualan];

        $model->id_penjualan = Penjualan::createID();
        $model->dari_penawaran = '0';
        if ($model->load(Yii::$app->request->post()) && $komisi->load(Yii::$app->request->post())) {

            $komisi->id_penjualan = $model->id_penjualan;

            $itemPenjualan = Model::createMultiple(ItemPenjualan::classname());
            Model::loadMultiple($itemPenjualan, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid_komisi = $komisi->validate();
            $valid = Model::validateMultiple($itemPenjualan) && $valid && $valid_komisi;

            if ($valid) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    if ($flag = $model->save(false)) {
                        foreach ($itemPenjualan as $item) {
                            $item->id_penjualan = $model->id_penjualan;
                            if (! ($flag = $item->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $komisi->save();
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_penjualan]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'komisi' => $komisi,
            'itemPenjualan' => (empty($itemPenjualan)) ? [new ItemPenjualan] : $itemPenjualan
        ]);
    }

    public function actionCreateByPenawaran($id_penawaran = null)
    {
        if ($id_penawaran == null) $this->redirect('/penawaran/index');
        $modelPenawaran = $this->findModelPenawaran($id_penawaran);
        $model = new Penjualan;
        $komisi = new Komisi;

        foreach($modelPenawaran->itemPenawarans as $key => $value) {
            $itemPenjualan[$key] = new ItemPenjualan();
            $itemPenjualan[$key]->id_item_penjualan = $value->id_item_penawaran;
            $itemPenjualan[$key]->kode_item = $value->kode_item;
            $itemPenjualan[$key]->qty = $value->qty;
            $itemPenjualan[$key]->harga_satuan = $value->harga_satuan;
        } 

        $model->id_penjualan = Penjualan::createID();
        $model->id_customer = $modelPenawaran->id_customer;
        $model->pnp = $modelPenawaran->pnp;
        $model->dari_penawaran = '1';
        $model->id_penawaran = $modelPenawaran->id_penawaran;
        if ($model->load(Yii::$app->request->post()) && $komisi->load(Yii::$app->request->post())) {

            $itemPenjualan = Model::createMultiple(ItemPenjualan::classname());
            Model::loadMultiple($itemPenjualan, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid_komisi = $komisi->validate();
            $valid = Model::validateMultiple($itemPenjualan) && $valid && $valid_komisi;

            if ($valid) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    if ($flag = $model->save(false)) {
                        foreach ($itemPenjualan as $item) {
                            $item->id_penjualan = $model->id_penjualan;
                            if (! ($flag = $item->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_penjualan]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'komisi' => $komisi,
            'itemPenjualan' => (empty($itemPenjualan)) ? [new ItemPenjualan] : $itemPenjualan
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $komisi = $this->findModelKomisi($id);
        $itemPenjualan = $model->itemPenjualans;

        if ($model->load(Yii::$app->request->post()) && $komisi->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($itemPenjualan, 'kode_item', 'kode_item');
            $itemPenjualan = Model::createMultiple(ItemPenjualan::classname(), $itemPenjualan, 'id_item_penjualan');
            Model::loadMultiple($itemPenjualan, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($itemPenjualan, 'id_item_penjualan', 'id_item_penjualan')));

            // validate all models
            $valid = $model->validate();
            $valid_komisi = $komisi->validate();
            $valid = Model::validateMultiple($itemPenjualan) && $valid && $valid_komisi;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            ItemPenjualan::deleteAll(['kode_item' => $deletedIDs]);
                        }
                        foreach ($itemPenjualan as $item) {
                            $item->id_penjualan = $model->id_penjualan;
                            if (! ($flag = $item->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $komisi->save();
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_penjualan]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'komisi' => $komisi,
            'itemPenjualan' => (empty($itemPenjualan)) ? [new ItemPenjualan] : $itemPenjualan
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    // public function actionCetakNota($id)
    // {
    //     return $this->render('cetak', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    protected function findModel($id)
    {
        if (($model = Penjualan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPenawaran($id)
    {
        if (($model = Penawaran::findOne($id)) !== null) {
            return $model;
        } else {
            return $this->redirect(['/penawaran/index']);
        }
    }

    protected function findModelKomisi($id)
    {
        if (($model = Komisi::findOne(["id_penjualan" => $id])) !== null) {
            return $model;
        } else {
            return new Komisi;
        }
    }
}
