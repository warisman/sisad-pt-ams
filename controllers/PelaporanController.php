<?php

namespace app\controllers;

use Yii;
use app\models\Pembelian;
use app\models\ItemPembelian;
use app\models\Penjualan;
use app\models\ItemPenjualan;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii2tech\spreadsheet\Spreadsheet;
use yii2tech\spreadsheet\SerialColumn;
use PhpOffice\PhpSpreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class XlsStyle
{
    public $header = [
        'font' => [
            'bold' => true,
            'size' => 13,
        ],
    ];
}

class PelaporanController extends Controller
{
    public function actionPembelian()
    {
        $startDate = Yii::$app->request->get('start', date('Y-m-d', strtotime('-6 day')));
        $endDate = Yii::$app->request->get('end', date('Y-m-d'));

        $query = Pembelian::find();
        $query->where(['between', 'tanggal', $startDate, $endDate]);
        $query->orderBy('tanggal DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => false,
        ]);

        return $this->render('pembelian', [
            'dataProvider' => $dataProvider,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }

    public function actionDownloadPembelian($id_pembelian = null)
    {
        // $spreadsheet = new PhpSpreadsheet\Spreadsheet();
        // $worksheet = $spreadsheet->getActiveSheet();
        // $filename = 'Laporan Pembelian ('.date('Y-m-d H:i:s').')';

        // $startDate = Yii::$app->request->get('start', date('Y-m-d', strtotime('-6 day')));
        // $endDate = Yii::$app->request->get('end', date('Y-m-d'));

        // $model = new Pembelian;
        // $header = $model->attributeLabels();
        // $model = $model::find();
        // $model->where(['between', 'tanggal', $startDate, $endDate]);
        // $model->orderBy('tanggal DESC');

        // $database = $model->all();

        // $worksheet->fromArray($header, null, 'A1');
        // $worksheet->fromArray(ArrayHelper::toArray($database), null, 'A2');

        // $writer = new Xlsx($spreadsheet);

        // header('Content-Type: application/vnd.ms-excel');
        // header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        // header('Cache-Control: max-age=0');
        // $writer->save('php://output');
        if (!$id_pembelian) {
            $startDate = Yii::$app->request->get('start', date('Y-m-d', strtotime('-6 day')));
            $endDate = Yii::$app->request->get('end', date('Y-m-d'));
            
            $query = Pembelian::find();
            $query->where(['between', 'tanggal', $startDate, $endDate]);
            $query->orderBy('tanggal DESC');
            
            $xls = new Spreadsheet([
                'dataProvider' => new ActiveDataProvider([
                    'query' => $query,
                ]),
            ]);
            $xls->applyColSize(['A','B','C','D','E','F','G','H','I','J'], 'auto');

            return $xls->send('Laporan Pembelian ('.date('Y-m-d H:i:s').').xls');
        } else {
            $query = Pembelian::find()->where(['id_pembelian' => $id_pembelian])->limit(1);
            $itemModel = new ItemPembelian;
            $data = $query->all()[0];

            $xls = new Spreadsheet(['dataProvider' => new ArrayDataProvider()]);
            $xStyle = new XlsStyle;
            $xls->applyColSize(['A','B','C','D','E'], 'auto');

            $xls->mergeCells('A1:B1');
            $xls->renderCell('A1', 'Detail Pembelian', $xStyle->header);
            $xls->renderCell('A2', $data->getAttributeLabel('id_pembelian'));
            $xls->renderCell('A3', $data->getAttributeLabel('kode_suplier'));
            $xls->renderCell('A4', $data->getAttributeLabel('tanggal'));
            $xls->renderCell('A5', $data->getAttributeLabel('no_faktur'));
            $xls->renderCell('A6', $data->getAttributeLabel('top_bel'));
            $xls->renderCell('A7', $data->getAttributeLabel('du_date'));
            $xls->renderCell('A8', $data->getAttributeLabel('dp'));
            $xls->renderCell('A9', $data->getAttributeLabel('pnp'));
            $xls->renderCell('A10', $data->getAttributeLabel('no_faktur_pajak'));

            $xls->renderCell('B2', $data['id_pembelian']);
            $xls->renderCell('B3', $data['kode_suplier']);
            $xls->renderCell('B4', $data['tanggal']);
            $xls->renderCell('B5', $data['no_faktur']);
            $xls->renderCell('B6', $data['top_bel']);
            $xls->renderCell('B7', $data['du_date']);
            $xls->renderCell('B8', $data['dp']);
            $xls->renderCell('B9', $data['pnp']);
            $xls->renderCell('B10', $data['no_faktur_pajak']);

            $xls->mergeCells('A12:B12');
            $xls->renderCell('A12', 'Detail Item Pembelian', $xStyle->header);

            $xls->renderFromArray('A13', $itemModel->attributeLabels());
            $xls->renderFromArray('A14', ArrayHelper::toArray($data->itemPembelians));

            return $xls->send('Laporan Detail Pembelian ('.$data->id_pembelian.').xls');
        }
    }

    public function actionPenjualan()
    {
        $startDate = Yii::$app->request->get('start', date('Y-m-d', strtotime('-6 day')));
        $endDate = Yii::$app->request->get('end', date('Y-m-d'));

        $query = Penjualan::find();
        $query->where(['between', 'tanggal', $startDate, $endDate]);
        $query->orderBy('tanggal DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => false,
        ]);

        return $this->render('penjualan', [
            'dataProvider' => $dataProvider,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }

    public function actionDownloadPenjualan($id_penjualan = null)
    {
        if (!$id_penjualan) {
            $startDate = Yii::$app->request->get('start', date('Y-m-d', strtotime('-6 day')));
            $endDate = Yii::$app->request->get('end', date('Y-m-d'));
            
            $query = Penjualan::find();
            $query->where(['between', 'tanggal', $startDate, $endDate]);
            $query->orderBy('tanggal DESC');
            
            $xls = new Spreadsheet([
                'dataProvider' => new ActiveDataProvider([
                    'query' => $query,
                ]),
            ]);
            $xls->applyColSize(['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P'], 'auto');

            return $xls->send('Laporan Penjualan ('.date('Y-m-d H:i:s').').xls');
        } else {
            $query = Penjualan::find()->where(['id_penjualan' => $id_penjualan])->limit(1);
            $itemModel = new ItemPenjualan;
            $data = $query->all()[0];

            $xls = new Spreadsheet(['dataProvider' => new ArrayDataProvider()]);
            $xStyle = new XlsStyle;
            $xls->applyColSize(['A','B','C','D','E'], 'auto');

            $xls->mergeCells('A1:B1');
            $xls->renderCell('A1', 'Detail Penjualan', $xStyle->header);
            $xls->renderCell('A2', $data->getAttributeLabel('id_penjualan'));
            $xls->renderCell('A3', $data->getAttributeLabel('id_customer'));
            $xls->renderCell('A4', $data->getAttributeLabel('pnp'));
            $xls->renderCell('A5', $data->getAttributeLabel('po'));
            $xls->renderCell('A6', $data->getAttributeLabel('tanggal'));
            $xls->renderCell('A7', $data->getAttributeLabel('dp'));
            $xls->renderCell('A8', $data->getAttributeLabel('tempo_bayar'));
            $xls->renderCell('A9', $data->getAttributeLabel('dari_penawaran'));
            $xls->renderCell('A10', $data->getAttributeLabel('id_penawaran'));
            $xls->renderCell('A11', $data->getAttributeLabel('diskon'));
            $xls->renderCell('A12', $data->getAttributeLabel('biaya_admin'));
            $xls->renderCell('A13', $data->getAttributeLabel('biaya_transport'));
            $xls->renderCell('A14', $data->getAttributeLabel('ongkos_kirim'));
            $xls->renderCell('A15', $data->getAttributeLabel('bonta'));
            $xls->renderCell('A16', $data->getAttributeLabel('pph_final'));
            $xls->renderCell('A17', $data->getAttributeLabel('profit'));

            $xls->renderCell('B2', $data['id_penjualan']);
            $xls->renderCell('B3', $data['id_customer']);
            $xls->renderCell('B4', $data['pnp']);
            $xls->renderCell('B5', $data['po']);
            $xls->renderCell('B6', $data['tanggal']);
            $xls->renderCell('B7', $data['dp']);
            $xls->renderCell('B8', $data['tempo_bayar']);
            $xls->renderCell('B9', $data['dari_penawaran']);
            $xls->renderCell('B10', $data['id_penawaran']);
            $xls->renderCell('B11', $data['diskon']);
            $xls->renderCell('B12', $data['biaya_admin']);
            $xls->renderCell('B13', $data['biaya_transport']);
            $xls->renderCell('B14', $data['ongkos_kirim']);
            $xls->renderCell('B15', $data['bonta']);
            $xls->renderCell('B16', $data['pph_final']);
            $xls->renderCell('B17', $data['profit']);

            $xls->mergeCells('A19:B19');
            $xls->renderCell('A19', 'Detail Item Penjualan', $xStyle->header);

            $xls->renderFromArray('A20', $itemModel->attributeLabels());
            $xls->renderFromArray('A21', ArrayHelper::toArray($data->itemPenjualans));

            return $xls->send('Laporan Detail Penjualan ('.$data->id_penjualan.').xls');
        }
    }

    public function actionPiutang()
    {
        return $this->render('piutang', []);
    }

    public function actionAnalisa()
    {
        return $this->render('analisa', []);
    }
}
