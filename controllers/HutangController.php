<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class HutangController extends Controller
{
    public function actionIndex()
    {
        // $searchModel = new HutangSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
        ]);
    }
}
