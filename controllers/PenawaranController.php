<?php

namespace app\controllers;

use Yii;
use app\models\Penawaran;
use app\models\ItemPenawaran;
use app\models\search\PenawaranSearch;
use app\components\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PenawaranController implements the CRUD actions for Penawaran model.
 */
class PenawaranController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Penawaran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PenawaranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Penawaran model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Penawaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Penawaran;
        $itemPenawaran = [new ItemPenawaran];

        $model->id_penawaran = Penawaran::createID();
        if ($model->load(Yii::$app->request->post())) {
            
            $itemPenawaran = Model::createMultiple(ItemPenawaran::classname());
            Model::loadMultiple($itemPenawaran, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($itemPenawaran) && $valid;

            if ($valid) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    if ($flag = $model->save(false)) {
                        foreach ($itemPenawaran as $item) {
                            $item->id_penawaran = $model->id_penawaran;
                            if (! ($flag = $item->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_penawaran]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'itemPenawaran' => (empty($itemPenawaran)) ? [new ItemPenawaran] : $itemPenawaran
        ]);
    }

    /**
     * Updates an existing Penawaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $itemPenawaran = $model->itemPenawarans;

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($itemPenawaran, 'kode_item', 'kode_item');
            $itemPenawaran = Model::createMultiple(ItemPenawaran::classname(), $itemPenawaran, 'id_item_penjualan');
            Model::loadMultiple($itemPenawaran, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($itemPenawaran, 'id_item_penjualan', 'id_item_penjualan')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($itemPenawaran) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            ItemPenawaran::deleteAll(['kode_item' => $deletedIDs]);
                        }
                        foreach ($itemPenawaran as $item) {
                            $item->id_penjualan = $model->id_penjualan;
                            if (! ($flag = $item->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_penjualan]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'itemPenawaran' => (empty($itemPenawaran)) ? [new ItemPenawaran] : $itemPenawaran
        ]);
    }

    /**
     * Deletes an existing Penawaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Penawaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Penawaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Penawaran::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
