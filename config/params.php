<?php

return [
    'adminEmail' => 'admin@pt-ams.com',
    'logo' => '/img/logo.png',
    'icon' => '/img/ams.png',
];
