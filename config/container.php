<?php

\Yii::$container->set('yii\grid\ActionColumn', [
    'contentOptions' => [
        'class' => 'text-center',
        'style' => [
            'white-space' => 'nowrap',
            'vertical-align' => 'middle',
        ],
    ],
]);

class aturan {

    public static function collapsed_menu_pages() {
        return [
            'penjualan/create',
            'pembelian/create',
            'penawaran/create',
        ];
    }

}