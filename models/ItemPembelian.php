<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_pembelian".
 *
 * @property int $id_item_pembelian
 * @property string $id_pembelian
 * @property string $kode_item
 * @property int $qty
 * @property string $harga_satuan
 *
 * @property Item $kodeItem
 * @property Pembelian $pembelian
 */
class ItemPembelian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_pembelian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_item', 'qty', 'harga_satuan'], 'required'],
            [['qty'], 'integer'],
            [['harga_satuan'], 'number'],
            [['id_pembelian', 'kode_item'], 'string', 'max' => 6],
            [['kode_item'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['kode_item' => 'kode_item']],
            [['id_pembelian'], 'exist', 'skipOnError' => true, 'targetClass' => Pembelian::className(), 'targetAttribute' => ['id_pembelian' => 'id_pembelian']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_item_pembelian' => 'Id Item Pembelian',
            'id_pembelian' => 'Id Pembelian',
            'kode_item' => 'Kode Item',
            'qty' => 'Qty',
            'harga_satuan' => 'Harga Satuan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodeItem()
    {
        return $this->hasOne(Item::className(), ['kode_item' => 'kode_item']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembelian()
    {
        return $this->hasOne(Pembelian::className(), ['id_pembelian' => 'id_pembelian']);
    }
}
