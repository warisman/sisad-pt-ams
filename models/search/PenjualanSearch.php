<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Penjualan;

/**
 * PenjualanSearch represents the model behind the search form of `app\models\Penjualan`.
 */
class PenjualanSearch extends Penjualan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_penjualan', 'pnp', 'po', 'tanggal', 'tempo_bayar', 'dari_penawaran'], 'safe'],
            [['id_customer'], 'integer'],
            [['dp', 'diskon', 'biaya_admin', 'biaya_transport', 'ongkos_kirim', 'bonta', 'pph_final', 'profit'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Penjualan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_customer' => $this->id_customer,
            'tanggal' => $this->tanggal,
            'dp' => $this->dp,
            'diskon' => $this->diskon,
            'biaya_admin' => $this->biaya_admin,
            'biaya_transport' => $this->biaya_transport,
            'ongkos_kirim' => $this->ongkos_kirim,
            'bonta' => $this->bonta,
            'pph_final' => $this->pph_final,
            'profit' => $this->profit,
        ]);

        $query->andFilterWhere(['like', 'id_penjualan', $this->id_penjualan])
            ->andFilterWhere(['like', 'pnp', $this->pnp])
            ->andFilterWhere(['like', 'po', $this->po])
            ->andFilterWhere(['like', 'tempo_bayar', $this->tempo_bayar])
            ->andFilterWhere(['like', 'dari_penawaran', $this->dari_penawaran]);

        return $dataProvider;
    }
}
