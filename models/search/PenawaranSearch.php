<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Penawaran;

/**
 * PenawaranSearch represents the model behind the search form of `app\models\Penawaran`.
 */
class PenawaranSearch extends Penawaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_penawaran', 'tanggal', 'tempo_bayar', 'pnp'], 'safe'],
            [['id_customer'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Penawaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_customer' => $this->id_customer,
            'tanggal' => $this->tanggal,
        ]);

        $query->andFilterWhere(['like', 'id_penawaran', $this->id_penawaran])
            ->andFilterWhere(['like', 'tempo_bayar', $this->tempo_bayar])
            ->andFilterWhere(['like', 'pnp', $this->pnp]);

        return $dataProvider;
    }
}
