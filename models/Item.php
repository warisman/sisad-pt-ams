<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item".
 *
 * @property string $kode_item
 * @property string $nama_item
 * @property string $satuan
 * @property string $kemasan
 * @property string $harga_beli
 * @property string $harga_jual
 * @property string $keterangan
 *
 * @property ItemPembelian[] $itemPembelians
 * @property ItemPenawaran[] $itemPenawarans
 * @property ItemPenjualan[] $itemPenjualans
 * @property SuplierItem[] $suplierItems
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_item', 'nama_item', 'satuan', 'kemasan'], 'required'],
            [['harga_beli', 'harga_jual'], 'number'],
            [['kode_item', 'satuan', 'kemasan'], 'string', 'max' => 5],
            [['nama_item'], 'string', 'max' => 100],
            [['keterangan'], 'string', 'max' => 150],
            [['kode_item'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode_item' => 'Kode Item',
            'nama_item' => 'Nama Item',
            'satuan' => 'Satuan',
            'kemasan' => 'Kemasan',
            'harga_beli' => 'Harga Beli',
            'harga_jual' => 'Harga Jual',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemPembelians()
    {
        return $this->hasMany(ItemPembelian::className(), ['kode_item' => 'kode_item']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemPenawarans()
    {
        return $this->hasMany(ItemPenawaran::className(), ['kode_item' => 'kode_item']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemPenjualans()
    {
        return $this->hasMany(ItemPenjualan::className(), ['kode_item' => 'kode_item']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuplierItems()
    {
        return $this->hasMany(SuplierItem::className(), ['kode_item' => 'kode_item']);
    }
}
