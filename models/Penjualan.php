<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penjualan".
 *
 * @property string $id_penjualan
 * @property int $id_customer
 * @property string $pnp
 * @property string $po
 * @property string $tanggal
 * @property string $dp
 * @property string $tempo_bayar
 * @property string $dari_penawaran
 * @property string $id_penawaran
 * @property string $diskon
 * @property string $biaya_admin
 * @property string $biaya_transport
 * @property string $ongkos_kirim
 * @property string $bonta
 * @property string $pph_final
 * @property string $profit
 *
 * @property ItemPenjualan[] $itemPenjualans
 * @property Komisi[] $komisis
 * @property Customer $customer
 */
class Penjualan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penjualan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_penjualan', 'id_customer', 'pnp', 'po', 'tanggal', 'dp', 'tempo_bayar', 'dari_penawaran', 'diskon', 'biaya_admin', 'biaya_transport', 'ongkos_kirim', 'bonta', 'pph_final', 'profit'], 'required'],
            [['id_customer'], 'integer'],
            [['tanggal', 'id_penawaran'], 'safe'],
            [['dp', 'diskon', 'biaya_admin', 'biaya_transport', 'ongkos_kirim', 'bonta', 'pph_final', 'profit'], 'number'],
            [['id_penjualan'], 'string', 'max' => 6],
            [['pnp', 'dari_penawaran'], 'string', 'max' => 1],
            [['po'], 'string', 'max' => 20],
            [['tempo_bayar'], 'string', 'max' => 3],
            [['id_penjualan'], 'unique'],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id_customer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_penjualan' => 'ID Penjualan',
            'id_customer' => 'ID Customer',
            'id_penawaran' => 'ID Penawaran',
            'pnp' => 'PNP',
            'po' => 'PO',
            'tanggal' => 'Tanggal',
            'dp' => 'DP',
            'tempo_bayar' => 'Tempo Bayar',
            'dari_penawaran' => 'Dari Penawaran',
            'diskon' => 'Diskon',
            'biaya_admin' => 'Biaya Admin',
            'biaya_transport' => 'Biaya Transport',
            'ongkos_kirim' => 'Ongkos Kirim',
            'bonta' => 'Bonta',
            'pph_final' => 'PPH Final',
            'profit' => 'Profit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemPenjualans()
    {
        return $this->hasMany(ItemPenjualan::className(), ['id_penjualan' => 'id_penjualan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomisis()
    {
        return $this->hasMany(Komisi::className(), ['id_penjualan' => 'id_penjualan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id_customer' => 'id_customer']);
    }

    public static function createID()
    {
        $n = self::find()->count() + 1;
        $id = 'J'.str_pad($n, 5, 0, STR_PAD_LEFT);
        return $id;
    }
}
