<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penawaran".
 *
 * @property string $id_penawaran
 * @property int $id_customer
 * @property string $tanggal
 * @property string $tempo_bayar
 * @property string $pnp
 *
 * @property ItemPenawaran[] $itemPenawarans
 * @property Customer $customer
 */
class Penawaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penawaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_penawaran', 'tanggal', 'tempo_bayar', 'pnp'], 'required'],
            [['id_customer'], 'integer'],
            [['tanggal'], 'safe'],
            [['id_penawaran'], 'string', 'max' => 6],
            [['tempo_bayar'], 'string', 'max' => 3],
            [['pnp'], 'string', 'max' => 1],
            [['id_penawaran'], 'unique'],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id_customer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_penawaran' => 'ID Penawaran',
            'id_customer' => 'ID Customer',
            'tanggal' => 'Tanggal',
            'tempo_bayar' => 'Tempo Bayar',
            'pnp' => 'PNP',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemPenawarans()
    {
        return $this->hasMany(ItemPenawaran::className(), ['id_penawaran' => 'id_penawaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id_customer' => 'id_customer']);
    }

    public static function createID()
    {
        $n = self::find()->count() + 1;
        $id = 'T'.str_pad($n, 5, 0, STR_PAD_LEFT);
        return $id;
    }
}
