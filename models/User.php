<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 */
class User extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['date_created', 'date_modified'], 'safe'],
            [['username'], 'string', 'max' => 200],
            [['password', 'access_token', 'auth_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'Username',
            'password' => 'Password',
            'access_token' => 'Access Token',
            'auth_key' => 'Auth Key',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
                $this->access_token = Yii::$app->security->generateRandomString() . '_' . time();
            }
            return true;
        }
        return false;
    }

    public static function findIdentity($user_id)
    {
        return self::findOne($user_id);
    }

    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->user_id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function generateAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function getAccess()
    {
        $acc = implode(", ", array_keys(Yii::$app->authManager->getAssignments($this->user_id)));
        return $acc;
    }

    public static function getAuthItems()
    {
        $manager = Yii::$app->getAuthManager();
        foreach (array_keys($manager->getPermissions()) as $name) {
            if ($manager->getPermissions()[$name]->data['admin_only'] != 'true') {
                if ($name[0] != '/') {
                    $permission[$name] = $name;
                }
            }
        }

        return[
            'permission' => $permission,
        ];
    }

    public static function getUserAssignments($user_id)
    {
        $assignments = [];
        $manager = Yii::$app->getAuthManager();
        foreach (array_keys($manager->getAssignments($user_id)) as $name) {
            if ($name[0] != '/') {
                $assignments[$name] = $name;
            }
        }

        return $assignments;
    }

    public static function revokeUserAssignments($user_id)
    {
        $manager = Yii::$app->getAuthManager();
        return $manager->revokeAll($user_id);
    }
}
