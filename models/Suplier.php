<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suplier".
 *
 * @property string $kode_suplier
 * @property string $nama_suplier
 * @property string $npwp
 * @property string $alamat
 * @property string $telp_fax
 * @property string $up
 *
 * @property Pembelian[] $pembelians
 * @property SuplierItem[] $suplierItems
 */
class Suplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_suplier'], 'required'],
            [['kode_suplier'], 'string', 'max' => 4],
            [['nama_suplier', 'telp_fax'], 'string', 'max' => 100],
            [['npwp'], 'string', 'max' => 15],
            [['alamat'], 'string', 'max' => 150],
            [['up'], 'string', 'max' => 45],
            [['kode_suplier'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode_suplier' => 'Kode Suplier',
            'nama_suplier' => 'Nama Suplier',
            'npwp' => 'Npwp',
            'alamat' => 'Alamat',
            'telp_fax' => 'Telp Fax',
            'up' => 'Up',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembelians()
    {
        return $this->hasMany(Pembelian::className(), ['kode_suplier' => 'kode_suplier']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuplierItems()
    {
        return $this->hasMany(SuplierItem::className(), ['kode_suplier' => 'kode_suplier']);
    }
}
