<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id_customer
 * @property string $kode
 * @property string $nama_customer
 * @property string $npwp
 * @property string $alamat
 * @property string $alamat_surat_jalan
 * @property string $telp
 * @property string $fax
 * @property string $email
 * @property string $to_
 * @property string $cc
 * @property string $contact_person
 * @property string $akronim
 *
 * @property Penawaran[] $penawarans
 * @property Penjualan[] $penjualans
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'nama_customer', 'npwp', 'alamat', 'alamat_surat_jalan', 'telp', 'fax', 'email', 'cc', 'contact_person', 'akronim'], 'required'],
            [['kode'], 'string', 'max' => 5],
            [['nama_customer'], 'string', 'max' => 100],
            [['npwp'], 'string', 'max' => 15],
            [['alamat', 'alamat_surat_jalan'], 'string', 'max' => 200],
            [['telp', 'fax'], 'string', 'max' => 25],
            [['email'], 'string', 'max' => 50],
            [['to_', 'cc', 'contact_person', 'akronim'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_customer' => 'Id Customer',
            'kode' => 'Kode',
            'nama_customer' => 'Nama Customer',
            'npwp' => 'Npwp',
            'alamat' => 'Alamat',
            'alamat_surat_jalan' => 'Alamat Surat Jalan',
            'telp' => 'Telp',
            'fax' => 'Fax',
            'email' => 'Email',
            'to_' => 'To',
            'cc' => 'Cc',
            'contact_person' => 'Contact Person',
            'akronim' => 'Akronim',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenawarans()
    {
        return $this->hasMany(Penawaran::className(), ['id_customer' => 'id_customer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenjualans()
    {
        return $this->hasMany(Penjualan::className(), ['id_customer' => 'id_customer']);
    }
}
