<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_penawaran".
 *
 * @property int $id_item_penawaran
 * @property string $id_penawaran
 * @property string $kode_item
 * @property int $qty
 * @property string $harga_satuan
 *
 * @property Penawaran $penawaran
 * @property Item $kodeItem
 */
class ItemPenawaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_penawaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_item', 'qty', 'harga_satuan'], 'required'],
            [['qty'], 'integer'],
            [['harga_satuan'], 'number'],
            [['id_penawaran'], 'string', 'max' => 6],
            [['kode_item'], 'string', 'max' => 5],
            [['id_penawaran'], 'exist', 'skipOnError' => true, 'targetClass' => Penawaran::className(), 'targetAttribute' => ['id_penawaran' => 'id_penawaran']],
            [['kode_item'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['kode_item' => 'kode_item']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_item_penawaran' => 'Id Item Penawaran',
            'id_penawaran' => 'Id Penawaran',
            'kode_item' => 'Kode Item',
            'qty' => 'Qty',
            'harga_satuan' => 'Harga Satuan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenawaran()
    {
        return $this->hasOne(Penawaran::className(), ['id_penawaran' => 'id_penawaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodeItem()
    {
        return $this->hasOne(Item::className(), ['kode_item' => 'kode_item']);
    }
}
