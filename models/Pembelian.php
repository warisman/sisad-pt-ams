<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pembelian".
 *
 * @property string $id_pembelian
 * @property string $kode_suplier
 * @property string $tanggal
 * @property string $no_faktur
 * @property string $top_bel
 * @property string $dp
 * @property string $pnp
 * @property string $no_faktur_pajak
 *
 * @property ItemPembelian[] $itemPembelians
 * @property Suplier $kodeSuplier
 */
class Pembelian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pembelian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pembelian', 'kode_suplier', 'tanggal', 'no_faktur', 'top_bel', 'dp', 'pnp', 'no_faktur_pajak'], 'required'],
            [['tanggal'], 'safe'],
            [['dp'], 'number'],
            [['id_pembelian'], 'string', 'max' => 6],
            [['kode_suplier'], 'string', 'max' => 4],
            [['no_faktur'], 'string', 'max' => 15],
            [['top_bel'], 'string', 'max' => 3],
            [['pnp'], 'string', 'max' => 1],
            [['no_faktur_pajak'], 'string', 'max' => 20],
            [['id_pembelian'], 'unique'],
            [['kode_suplier'], 'exist', 'skipOnError' => true, 'targetClass' => Suplier::className(), 'targetAttribute' => ['kode_suplier' => 'kode_suplier']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pembelian' => 'ID Pembelian',
            'kode_suplier' => 'Kode Suplier',
            'tanggal' => 'Tanggal',
            'no_faktur' => 'No Faktur',
            'top_bel' => 'TOP',
            'dp' => 'DP',
            'pnp' => 'PNP',
            'no_faktur_pajak' => 'No Faktur Pajak',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemPembelians()
    {
        return $this->hasMany(ItemPembelian::className(), ['id_pembelian' => 'id_pembelian']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodeSuplier()
    {
        return $this->hasOne(Suplier::className(), ['kode_suplier' => 'kode_suplier']);
    }

    public static function createID()
    {
        $n = self::find()->count() + 1;
        $id = 'P'.str_pad($n, 5, 0, STR_PAD_LEFT);
        return $id;
    }
}
