<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_penjualan".
 *
 * @property int $id_item_penjualan
 * @property string $id_penjualan
 * @property string $kode_item
 * @property string $harga_satuan
 * @property int $qty
 *
 * @property Item $kodeItem
 * @property Penjualan $penjualan
 */
class ItemPenjualan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_penjualan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_item', 'harga_satuan', 'qty'], 'required'],
            [['harga_satuan'], 'number'],
            [['qty'], 'integer'],
            [['id_penjualan'], 'string', 'max' => 6],
            [['kode_item'], 'string', 'max' => 6],
            [['kode_item'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['kode_item' => 'kode_item']],
            [['id_penjualan'], 'exist', 'skipOnError' => true, 'targetClass' => Penjualan::className(), 'targetAttribute' => ['id_penjualan' => 'id_penjualan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_item_penjualan' => 'Id Item Penjualan',
            'id_penjualan' => 'Id Penjualan',
            'kode_item' => 'Kode Item',
            'harga_satuan' => 'Harga Satuan',
            'qty' => 'Qty',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodeItem()
    {
        return $this->hasOne(Item::className(), ['kode_item' => 'kode_item']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenjualan()
    {
        return $this->hasOne(Penjualan::className(), ['id_penjualan' => 'id_penjualan']);
    }
}
