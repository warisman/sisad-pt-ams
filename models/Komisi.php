<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "komisi".
 *
 * @property int $id_komisi
 * @property string $id_penjualan
 * @property int $id_penerima
 * @property string $jumlah_persen
 * @property string $jumlah_nominal
 *
 * @property Penjualan $penjualan
 * @property PenerimaKomisi $penerima
 */
class Komisi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'komisi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_penjualan', 'id_penerima', 'jumlah_persen', 'jumlah_nominal'], 'required'],
            [['id_penerima'], 'integer'],
            [['jumlah_persen', 'jumlah_nominal'], 'number'],
            [['id_penjualan'], 'string', 'max' => 6],
            // [['id_penjualan'], 'exist', 'skipOnError' => true, 'targetClass' => Penjualan::className(), 'targetAttribute' => ['id_penjualan' => 'id_penjualan']],
            [['id_penerima'], 'exist', 'skipOnError' => true, 'targetClass' => PenerimaKomisi::className(), 'targetAttribute' => ['id_penerima' => 'id_penerima']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_komisi' => 'Id Komisi',
            'id_penjualan' => 'Id Penjualan',
            'id_penerima' => 'Id Penerima',
            'jumlah_persen' => 'Jumlah Persen',
            'jumlah_nominal' => 'Jumlah Nominal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenjualan()
    {
        return $this->hasOne(Penjualan::className(), ['id_penjualan' => 'id_penjualan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenerima()
    {
        return $this->hasOne(PenerimaKomisi::className(), ['id_penerima' => 'id_penerima']);
    }
}
