<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penerima_komisi".
 *
 * @property int $id_penerima
 * @property string $nama_penerima
 * @property string $npwp
 * @property string $telp
 * @property string $alamat
 *
 * @property Komisi[] $komisis
 */
class PenerimaKomisi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penerima_komisi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_penerima', 'npwp', 'telp', 'alamat'], 'required'],
            [['nama_penerima'], 'string', 'max' => 50],
            [['npwp', 'telp'], 'string', 'max' => 15],
            [['alamat'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_penerima' => 'Id Penerima',
            'nama_penerima' => 'Nama Penerima',
            'npwp' => 'Npwp',
            'telp' => 'Telp',
            'alamat' => 'Alamat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomisis()
    {
        return $this->hasMany(Komisi::className(), ['id_penerima' => 'id_penerima']);
    }
}
